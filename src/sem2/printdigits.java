package sem2;

public class printdigits {
    public static void main(String[] args) {
        printDigit(1234);
    }

    public static void printDigit(int number) {
        if (number < 10) {
            System.out.println(number);
        } else {
            printDigit(number / 10);
            System.out.println(number % 10);
        }

    }
}
