package sem2;

public class chocolatewrappers {
    public static void main(String[] args) {
        int money = 15;
        int price = 1;
        int wrap = 3;

        System.out.println(countChocolates(money, price, wrap));
    }

    public static int countChocolates(int money, int price, int wrappers) {
        int chocolates = money/price;

        return chocolates + countChocolatesFromWrappers(chocolates, wrappers);
    }

    public static int countChocolatesFromWrappers(int chocolates, int wrappers) {
        if (chocolates < wrappers) {
            return 0;
        }

        int newChocolates = chocolates / wrappers;

        return newChocolates + countChocolatesFromWrappers(newChocolates + chocolates % wrappers, wrappers);
    }

}
